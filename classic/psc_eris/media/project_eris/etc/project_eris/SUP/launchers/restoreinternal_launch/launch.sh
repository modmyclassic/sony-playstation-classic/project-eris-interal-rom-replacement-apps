#!/bin/sh
source "/var/volatile/project_eris.cfg"
BACKUPFLAGFILE="${PROJECT_ERIS_PATH}/internal_games/.flags/backup_done.flag"
echo -n 2 > "/data/power/disable"
${PROJECT_ERIS_PATH}/bin/sdl_text_display "Attempting to restore internal games from backup, this takes approximately 12 minutes." &
sleep 4
if [ -f "${BACKUPFLAGFILE}" ]; then
  FLAGINT="$(cat ${BACKUPFLAGFILE})"
  if [ "${FLAGINT}" = "2" ] || [ "${FLAGINT}" = "3" ]; then
    if [ -d "${PROJECT_ERIS_PATH}/internal_games/games/" ]; then
      mount -o rw,remount "/gaadata"
      sleep 1
      for i in {1..25}; do
        if [ -d "/gaadata/${i}" ]; then
          rm -Rfv "/gaadata/${i}"
        fi
      done
      cp -Rfv "${PROJECT_ERIS_PATH}/internal_games/games/"* "/gaadata/" > "${PROJECT_ERIS_PATH}/internal_games/restoredfiles.log"
	    rm -fv "${PROJECT_ERIS_PATH}/etc/project_eris/SYS/databases/stockRegional.db"
	    cp -fv "${PROJECT_ERIS_PATH}/internal_games/games/databases/regional.db" "${PROJECT_ERIS_PATH}/etc/project_eris/SYS/databases/stockRegional.db"
      echo 1 > "${BACKUPFLAGFILE}"
      ${PROJECT_ERIS_PATH}/bin/sdl_text_display "Restoring internal games complete! Returning to stock UI in 5 seconds." &
      if [ "$SET_GAADATA_WRITABLE" != "1" ]; then
        mount -o remount,ro "/gaadata"
      fi
      sleep 5
    else
      echo 1 > "/sys/class/leds/red/brightness"
      echo 0 > /sys/class/leds/green/brightness
      ${PROJECT_ERIS_PATH}bin/sdl_text_display "There is no complete internal game backup folder on USB! Returning to stock UI in 5 seconds." &
      sleep 5
      echo 0 > "/sys/class/leds/red/brightness"
      echo 1 > /sys/class/leds/green/brightness
    fi
  else
    echo 1 > "/sys/class/leds/red/brightness"
    echo 0 > /sys/class/leds/green/brightness
    ${PROJECT_ERIS_PATH}bin/sdl_text_display "There is no valid value within the backup flag on USB! Returning to stock UI in 5 seconds." &
    sleep 4
    echo 0 > "/sys/class/leds/red/brightness"
    echo 1 > /sys/class/leds/green/brightness
  fi
else
  echo 1 > "/sys/class/leds/red/brightness"
  echo 0 > /sys/class/leds/green/brightness
  ${PROJECT_ERIS_PATH}bin/sdl_text_display "There is no backup flag on USB! Returning to stock UI in 5 seconds." &
  sleep 5
  echo 0 > "/sys/class/leds/red/brightness"
  echo 1 > /sys/class/leds/green/brightness
fi
echo -n 1 > "/data/power/disable"
echo "launch_stockui" > "/tmp/launchfilecommand"