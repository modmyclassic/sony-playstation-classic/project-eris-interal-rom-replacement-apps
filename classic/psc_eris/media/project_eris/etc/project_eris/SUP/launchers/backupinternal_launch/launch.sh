#!/bin/sh
source "/var/volatile/project_eris.cfg"
BACKUPFLAGFILE="${PROJECT_ERIS_PATH}/internal_games/.flags/backup_done.flag"
echo -n 2 > "/data/power/disable"
${PROJECT_ERIS_PATH}/bin/sdl_text_display "Backing up internal games, this takes approximately 12 minutes." &
sleep 4
cp -f "/gaadata/databases/regional.db" "${PROJECT_ERIS_PATH}/etc/project_eris/SYS/databases/internal_db_backup.db"
if [ ! -d "${PROJECT_ERIS_PATH}/internal_games" ]; then
  mkdir -p "${PROJECT_ERIS_PATH}/internal_games/games" "${PROJECT_ERIS_PATH}/internal_games/.flags"
  cp -Rfv "/gaadata/"* "${PROJECT_ERIS_PATH}/internal_games/games/" > "${PROJECT_ERIS_PATH}/internal_games/copiedfiles.log"
  echo "1" > "${BACKUPFLAGFILE}"
  df > "${PROJECT_ERIS_PATH}/internal_games/df.log"
  ${PROJECT_ERIS_PATH}/bin/sdl_text_display "Backing up internal games complete! Returning to stock UI in 5 seconds." &
  sleep 5
else
  echo 1 > "/sys/class/leds/red/brightness"
  echo 0 > /sys/class/leds/green/brightness
  ${PROJECT_ERIS_PATH}/bin/sdl_text_display "There is already an internal game backup folder on USB! Returning to stock UI in 5 seconds." &
  sleep 5
  echo 0 > "/sys/class/leds/red/brightness"
  echo 1 > /sys/class/leds/green/brightness
fi
echo -n 1 > "/data/power/disable"
echo "launch_stockui" > "/tmp/launchfilecommand"